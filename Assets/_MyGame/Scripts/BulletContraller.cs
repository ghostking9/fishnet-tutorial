﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishNet.Object;
using System;

public class BulletContraller : NetworkBehaviour
{
    private Rigidbody rig;
    public float force = 500;

    public float lifeTime = 5f;

    // Start is called before the first frame update
    private void Start()
    {
        rig = GetComponent<Rigidbody>();
        rig.AddForce(transform.forward * force);
    }

    // Update is called once per frame
    private void Update()
    {
        if (base.IsServer)
        {
            lifeTime -= Time.deltaTime;
            if (lifeTime < 0)
            {
                Destoryself();
            }
        }
    }

    [Server]
    private void Destoryself()
    {
        Despawn();
    }

    [Server]
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.GetComponent<HealthManager>().Damage(10);
        }

        Despawn();
    }
}