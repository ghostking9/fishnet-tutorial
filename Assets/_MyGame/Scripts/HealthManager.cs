﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using UnityEngine.UI;

public class HealthManager : NetworkBehaviour
{
    [SyncVar(OnChange = nameof(UpdateHealthBar))]
    private int HealthValue = 100;

    public Image HealthBar;

    private Animator _animator;

    private void Start()
    {
        _animator = GetComponent<Animator>();
    }

    private void UpdateHealthBar(int _oldValue, int _newValue, bool asServer)
    {
        HealthBar.fillAmount = (float)_newValue / 100;
    }

    [Server]
    public void Damage(int value)
    {
        if (HealthValue > 0)
        {
            HealthValue -= value;

            if (HealthValue <= 0)
            {
                //死。Die动画
                _animator.SetBool("Die", true);
            }
        }
    }
}