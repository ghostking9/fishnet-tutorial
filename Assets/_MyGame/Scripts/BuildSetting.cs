﻿using FishNet.Managing;
using FishNet.Transporting;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildSetting : MonoBehaviour
{
    private NetworkManager _networkManager;
    public GameObject ClientLoginUI;

    private void Start()
    {
        _networkManager = GetComponent<NetworkManager>();

#if UNITY_SERVER
          ClientLoginUI.SetActive(false);
       _networkManager.ServerManager.OnServerConnectionState += ServerManager_OnServerConnectionState;
        _networkManager.ServerManager.StartConnection();

#else
        ClientLoginUI.SetActive(true);
        _networkManager.ClientManager.OnClientConnectionState += ClientManager_OnClientConnectionState;
#endif
    }

#if UNITY_SERVER
        private void ServerManager_OnServerConnectionState(ServerConnectionStateArgs obj)
        {
            print("===== Server State="+ obj.ConnectionState.ToString());
        }

#else

    private void ClientManager_OnClientConnectionState(ClientConnectionStateArgs obj)
    {
        Debug.Log("Client State=" + obj.ConnectionState.ToString());
    }

    public void ClientLogin()
    {
        _networkManager.ClientManager.StartConnection();
        ClientLoginUI.SetActive(false);
    }

#endif

    private void OnDestroy()
    {
        if (_networkManager == null)
            return;

#if UNITY_SERVER
        _networkManager.ServerManager.OnServerConnectionState -= ServerManager_OnServerConnectionState;
#else
        _networkManager.ClientManager.OnClientConnectionState -= ClientManager_OnClientConnectionState;
#endif
    }
}